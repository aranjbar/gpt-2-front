const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

// Compile SCSS files from /sass into /css
gulp.task('sass', () => {
    return gulp.src('./sass/*.scss')
        .pipe(sass({
            errLogToConsole: false,
        }))
        .on('error', (err) => {
            console.log(err);
            this.emit('end');
        })
        .pipe(gulp.dest('./css'));
});

// Configure the browserSync task
gulp.task('browserSync', () => {
    browserSync.init({
        files: ['./**/*.*'],
        server: {
            baseDir: './'
        },
        base: './index.html'
    });
});

// Dev task with browserSync
gulp.task('watch', ['browserSync', 'sass'], () => {
    gulp.watch('sass/*.scss', ['sass']);
});
